Le but de cette petite revue de littérature est d'identifier un prisme théorique capable de 
générer des prédictions testables. La négligence spatiale unilatérale (nsu) est une pathologie très
complexe car très hétérogène, survenant principalement après un AVC ciblant l'hémisphère droit. 
La nsu est un mélange de déficits latéralisés et non-latéralisés dans l'espace. 

Le déficit latéralisé fut le 1er décrit en détail, faisant référence à l'absence de perception et
d'exploration de l'hémi-espace contralésionnel (le gauche généralement), accompagné d'un biais comportemental
vers la droite. 
La nsu se distingue des déficits sensoriels primaires (hémi-anesthésie-plégie-anopie), en affectant des traitements
cognitifs de plus haut niveau, sur le plan perceptif. On parle plus précisément de déficit de perception spatiale, 
ou encore de déficit d'attention spatiale.

Les déficits attentionnels et perceptifs ne renvoient pas aux mêmes théories explicatives de la nsu. D'un point de vue 
comportemental, la nsu renvoie à une incapacité à capturer consciemment les caractéristiques immanentes d'objet ou d'évènement 
situés dans l'hémi-espace gauche. Or, le terme de perception spatiale doit être bien défini pour savoir si il rend
bien compte de ce phénomène. Reprenons les termes de Kitchin 1994 pour y voir plus clair.

La représentation spatiale peut être vue comme une copie en mémoire à long terme de la perception spatiale. Ce concept 
ressemble à celui de carte cognitive, défini comme une suite de traitements cognitifs qui codent les informations de
notre environnement sur la position relative des objets. On code la position d'un objet par rapport à soi, un autre objet,
ou la gravité (voir Prévic 1998). 
Selon Kitchin, la carte cognitive est un mélange de cognition spatiale et de cognition environnementale. La cognition
spatiale est la reconstruction mentale, interne, des propriétés de l'espace. La cognition environnementale renvoie aux 
impressions, images, croyances, portant sur l'environnement.
Kitchin cite Cohen qui résume la carte cognitive comme l'union de connaissances spatiales et sociales sur l'ev. Au sens de 
Tolman 48, la carte cognitive renvoie à une reconstruction en coordonnées cartésiennes de l'espace.
Donc en somme, une carte cognitive contient des informations portant sur la localisation des objects, mais aussi sur leur
caractéristiques.

point de vue sur le contenu de la *carte* cognitive :
- explicite : la carte est une carte, un modèle euclidien en 3d du monde. voir nadel et okefee ici. 
- analogique : la carte est comme une carte. 

Quid de l'oriention spatiale ? Capacité à savoir où nous sommes comparés à un repère. 

C'est pourquoi la nsu a été vue par des auteurs pionniers comme un déficit d'intégration multisensoriel sous-tendant la 
représentation de l'espace. 


- R de l'espace = cognition spatiale = cognitive map
- perception spatiale = perception de la position des objets par rapport à des repères. si nsu, pb de traitement de cette
position, par exemple, objet dont la position était traitée à gauche de l'axe médian du corps n'est plus perçu à cet endroit,
mais beaucoup plus à gauche qu'avant. pour jeanneron, ventre, etc je déplace mon référentiel à gauche, et donc je biaise le 
codage de la perception des objets vers la droite. les objets à gauche sont alors négligés, car leur position n'est pas codée
convenablement, malgré inputs visuel normal.
- traitement spatial = égo, allo, qui repose sur mss et pcp de plusieurs cadres de réf, par ex, cadre de réf eye-centré = vision
only, alors que tete centré nécessite inputs ppc de l'oeil. 

1
--> une approche pcp classique pas vraiment en accord avec ce point de vue ? ou partiellement ? par ex, peut être que
déviation vers la droite est une csq de ne plus percevoir l'espace gauche.

colle bien avec le profil des lésions impliquées dans la nsu, portant souvent sur des régions ventro-dorsales telles que le ppc,
le stg, ou l'insula, contenant des neurones multi-sensoriels. 

Jacobs et al. (2012) mettent en avant l'idée que la nsu est un problème d'intégration multi-sensoriel
sous-tendant la perception de l'espace. Ses arguments sont que la nsu peut toucher plusieurs modalités sensorielles à la fois
chez un même individu. De plus, certains travaux montrent des corrélations entre le biais perceptif dans plusieurs
modalités. Par exemple, Shindler 2006 observe une corrélation entre le biais de bissection visuelle et de 
bissection tactile. Kerkhoff 1999 a aussi montré que la perception de la verticalité, grandement affectée chez les patients
avec nsu comparé à sans nsu, se retrouve dans plusieurs modalités. Ainsi, les patients atteints de nsu perçoivent 
une barre lumineuse orientée vers gauche dans le plan frontal dans la tâche de verticale visuelle. 
Tout comme Schindler 2006, Kerkhoff 1999 a observé une corrélation très forte entre des tâches de verticalité impliquant les
modalités visuelles et haptiques. Des travaux récents de notre équipe soutient ces travaux, puisque nous avons observé
une corrélation similaire entre le biais de verticale visuelle et le biais de verticale postural, qui mobilise respectivement
les afférences vestibulo-visuelles et vestibulo-somesthésiques. 
